# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.
"""
A Bitbucket Builds template for deploying an application to AWS Lambda
joshcb@amazon.com
v1.0.0
Updated by tempo810@gmail.com for parsing multiple AWS account credentials
"""

# Remark! This script run on BitBucket pipeline, not AWS lambda
from __future__ import print_function
import sys
import ast
import boto3
from botocore.exceptions import ClientError
from multiprocessing.pool import ThreadPool


def publish_new_version(artifact, func_name, acc_info):
    """
    Publishes new version of the AWS Lambda Function
    """
    try:
        client = boto3.client('lambda',
                              aws_access_key_id=acc_info['AWS_ACCESS_KEY_ID'],
                              aws_secret_access_key=acc_info['AWS_SECRET_ACCESS_KEY'],
                              region_name=acc_info['AWS_DEFAULT_REGION'])
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.update_function_code(
            FunctionName=func_name,
            ZipFile=open(artifact, 'rb').read(),
            Publish=True
        )
        return response
    except ClientError as err:
        print("Failed to update function code.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + artifact + ".\n" + str(err))
        return False


def main(func_name, acc_list):
    " Your favorite wrapper's favorite wrapper "
    pool = ThreadPool(len(acc_list))
    results_list = []
    for item in acc_list:
        results_list.append(pool.apply_async(publish_new_version, ('/tmp/artifact.zip', func_name, item)))
    results_list = [r.get() for r in results_list]
    pool.close()
    pool.join()
    if False in results_list:
        sys.exit(1)


if __name__ == "__main__":

    account_list = [ast.literal_eval(sys.argv[i + 2])
                    for i in range(len(sys.argv) - 2)]
    if not all(k in item for item in account_list for k in (
            'AWS_ACCESS_KEY_ID',
            'AWS_SECRET_ACCESS_KEY',
            'AWS_DEFAULT_REGION')):
        print("AWS account variable validate error.")
        sys.exit(1)

    main(func_name=sys.argv[1],
         acc_list=account_list)
